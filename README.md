# MPC
Commit 30c05de0

# applications
This directory contains the microbenchmark and the task-based version of LULESH.

# raw-data
This directory contains raw output files from experiments presented in the paper.

# mpc.patch
MPC and libomptarget inteoperability enable automatic overlap CPU/GPU synchronization
operations through cooperative task-switches on cores.
To enable the interoperability layer, one should
- checkout to MPC commit `30c05de0` - and install using `[MPC]/installmpc --prefix=[PREFIX] --mpcframework="--enable-debug --enable-openmp --disable-lowcomm --disable-mpi --disable-optim --enable-debug --disable-mpcalloc --enable-process-mode --with-slurm" --with-slurm --with-pmix=${PMIX_ROOT} --disable-mpc-comp  --process-mode`
- checkout to LLVM commit `0e27d08c` (branch release/14.x)
- apply `mpc.patch` to LLVM (`git apply mpc.patch` under LLVM source tree)
- install LLVM libomptarget :
    - adding `-DUSE_MPC_OMP_INTEROP=ON` to the cmake command
    - setting `MPC_INSTALL_DIR_PREFIX` environnement variable (which is set by default after sourcing `mpcvars.sh`)

Then, using this installation, one would link its application using
```
mpc_cxx -cc=clang++ -Wall -Wextra -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda -Xopenmp-target -march=sm_[XX] main.c -o main
```
