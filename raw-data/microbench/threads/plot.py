import csv
import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if len(sys.argv) < 2:
    print("usage: {} [stats.csv]".format(sys.argv[0]))
    sys.exit(1)

###############################################
# CURVES
###############################################
df = pd.read_csv(sys.argv[1], sep=":")
print(df)

llvm=df[df['app'] == 'main-llvm'].reset_index()
mpc=df[df['app'] == 'main-mpc'].reset_index()
nv=df[df['app'] == 'main-nv'].reset_index()

print("Max performance")
print(df.iloc[df[df['app'] == 'main-llvm']['gflops'].idxmax()])
print(df.iloc[df[df['app'] ==  'main-mpc']['gflops'].idxmax()])
print(df.iloc[df[df['app'] ==   'main-nv']['gflops'].idxmax()])

x=np.arange(4, 16+1)
xi=[2**i for i in x]

d = df.groupby(["app", "threads", "n", "t"]).agg({'time': (np.min, np.median, np.max)}).reset_index()
ref = "main-llvm"
dref_threads = 8
dref = d[(d["app"] == ref) & (d["threads"] == dref_threads)][["n", "threads", "time"]].groupby(["n"]).agg({('time', 'median'): np.min}).reset_index()
#dref = d[d["app"] == ref][["n", "threads", "time"]].groupby(["n"]).agg({('time', 'median'): np.min}).reset_index()
configs = [
    {'threads':  2, 'style':  '-', 'marker': 'o'},
    {'threads':  4, 'style': '--', 'marker': 'v'},
    {'threads':  8, 'style': '-.', 'marker': '1'},
    #{'threads': 12, 'style':  ':', 'marker': 's'}
    {'threads': 16, 'style':  ':', 'marker': 's'}
]
for app in ('main-llvm', 'main-mpc', 'main-nv'):
    fig, ax = plt.subplots()
    for config in configs:

        dapp = d[(d['threads'] == config['threads']) & (d['app'] == app)][['n', 'time']].reset_index()
        dapp['speedup'] = dref[('time', 'median')].div(dapp[('time', 'median')])
        dapp['yerr-min'] = abs(dapp['speedup'] - dref[('time', 'median')].div(dapp[('time', 'amax')]))
        dapp['yerr-max'] = abs(dapp['speedup'] - dref[('time', 'median')].div(dapp[('time', 'amin')]))
        ax.errorbar(dapp['n'], dapp['speedup'], linestyle=config['style'], marker=config['marker'], yerr=(dapp['yerr-min'], dapp['yerr-max']))
    ax.legend(["{} threads".format(config['threads']) for config in configs])
    ax.set_title("B5 " + app)
    ax.axhline(y=1.0, color='black', linestyle='--')
    ax.set_xlabel("N")
    ax.set_ylabel("Speedup")
    ax.set_xscale('log', basex=2)
    ax.set_xticks(xi)
    ax.set_xticklabels(["$2^{" + str(i) + "}$" for i in x])
    fig.set_size_inches(5, 4)

plt.show()
