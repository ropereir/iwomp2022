import csv
import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if len(sys.argv) < 2:
    print("usage: {} [stats.csv]".format(sys.argv[0]))
    sys.exit(1)

###############################################
# CURVES
###############################################
df = pd.read_csv(sys.argv[1], sep=":")
df = df[df['n'] == 1]
print(df)

d = df.groupby(["mode", "c", "size", "st"]).agg({'time': (np.min, np.median, np.max), 'gen': (np.min, np.median, np.max)}).reset_index()
print(d)

fig, ax = plt.subplots()

legend = []

# for version
dfor = d[d['mode'] == 'lulesh-omp-for-clang++']
ax.axhline(y=dfor['time']['median'].iloc[0], linestyle='-.')
legend.append('parallel-for')

# task CPU version
dcpu = d[d['mode'] == 'lulesh-mpc-t-clang++-cpu']
ax.axhline(y=dcpu['time']['median'].iloc[0], color='red', linestyle='--')
legend.append('task-based (no offload)')
#ax.axhline(y=dcpu[ 'gen']['median'].iloc[0], color='g', linestyle=':')
#legend.append('TDG generation (no offload)')

# task GPU version
dgpu = d[d['mode'] == 'lulesh-mpc-t-clang++-gpu']
x=dgpu['st']
y=dgpu['time']['median']
ymin=dgpu['time']['amin']
ymax=dgpu['time']['amax']
ax.plot(x, y, color='green', linestyle=(0, (3, 1, 1, 1, 1, 1)))
ax.fill_between(x, ymin, ymax, alpha=0.5, color='green')
legend.append('task-based (with offload)')

#y=dgpu['gen']['median']
#ax.plot(x, y)
#legend.append('TDG generation (offload)')

# task GPU version with no overlap
#dgpu = d[d['mode'] == 'lulesh-mpc-t-clang++-gpu-no-overlap']
#x=dgpu['st']
#y=dgpu['time']['median']
#ymin=dgpu['time']['amin']
#ymax=dgpu['time']['amax']
#ax.plot(x, y, color="#bf7e04")
#ax.fill_between(x, ymin, ymax, alpha=0.5)
#legend.append('TDG execution (offload, no cooperativity)')

ax.legend(legend)
#ax.set_title('GPU target task grain study')
ax.set_xlabel('-st')
ax.set_ylabel('Time (in s.)')
ax.set_ylim(0, 150)
#ax.set_ylim(74, 84.5)
ax.set_xscale('log', basex=2)

fig.set_size_inches(6, 4)

plt.show()
