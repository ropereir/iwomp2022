import csv
import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if len(sys.argv) < 2:
    print("usage: {} [stats.csv]".format(sys.argv[0]))
    sys.exit(1)

###############################################
# CURVES
###############################################
df = pd.read_csv(sys.argv[1], sep=":")
print(df)

d = df.groupby(["mode", "N", "n", "c", "size", "st"]).agg({'time': (np.min, np.median, np.max), 'gen': (np.min, np.median, np.max)}).reset_index()


pf=[4, 4, 4]
cpu=[5, 5, 5]
gpu=[6, 6, 6]

apps=[
    {
        'bin': 'lulesh-omp-mpi-for-clang++',
        'values': [],
        'label': 'parallel-for',
        'pattern': 'o'
    },
    {
        'bin': 'lulesh-mpc-mpi-ext-t-cpu',
        'values': [],
        'label': 'CPU',
        'pattern': '/'
    },
    {
        'bin': 'lulesh-mpc-mpi-ext-t-gpu',
        'values': [],
        'label': 'GPU',
        'pattern': 'x'
    },
#    {
#        'bin': 'lulesh-mpc-mpi-ext-t-gpu-no-overlap',
#        'values': [],
#        'label': 'GPU (no coop.)',
#        'pattern': '\\'
#    }
]

width=1
fig, ax = plt.subplots()

plt.rcParams['hatch.linewidth'] = 2
plt.rcParams.update({'hatch.color': '#555555'})

print("n in [1, 8, 27]")
labels=[]
for i, app in enumerate(apps):
    dapp = d[d['mode'] == app['bin']]
    values = []
    errmin = []
    errmax = []
    for n in (1, 8, 27):
        m   = dapp[dapp['n'] == n][('time', 'amin')].iloc[0]
        med = dapp[dapp['n'] == n][('time', 'median')].iloc[0]
        M   = dapp[dapp['n'] == n][('time', 'amax')].iloc[0]
        values.append((m, med, M))
        app['values'].append(med)
        errmin.append(abs(med - m))
        errmax.append(abs(med - M))
    err=(errmin, errmax)
    bars=ax.bar([j*width-(i-1)*0.22*width for j in range(3)], app['values'],  width=width*0.2, yerr=err, error_kw=dict(ecolor='black', lw=2, capsize=5, capthick=2))
    for bar in bars:
        bar.set_hatch(app['pattern'])
    labels.append(app['label'])
    print('----------')
    print(app['label'])
    print(values)


ax.set_xticks([i*width for i in range(3)])
ax.set_xticklabels(['1', '8', '27'])

ax.set_xlabel('Number of MPI rank')
ax.set_ylabel('Time (in s.)')

ax.legend(labels)
ax.set_axisbelow(True)
ax.yaxis.grid(color='gray', linestyle='dashed')
plt.show()
