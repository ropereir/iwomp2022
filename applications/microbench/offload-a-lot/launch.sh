#!/bin/sh

module load nvhpc/22.2

# NUMBER OF MEASURES PER POINT
ITER0=0
ITERN=5

#app=$1
app=./main-nv
root=$(pwd)

# enable tracing
trace=0
keyword="took"

export MPCFRAMEWORK_LAUNCH_DEBUG_VERBOSITY=1
export MPCFRAMEWORK_OMP_THREAD_BINDINGS=1

# Bench id, pageable/pinned memory, enable correctness checking
b=6
p=1
check=0

#for threads in 1 2 4 6 8 10 12 14 16 ; do
for threads in 2 4 8 12 16 ; do
    for en in {2..16} ; do
        #for t in 16 64 256 1024 ; do
        for t in 64 ; do
            for (( i=$ITER0; i < $ITERN; i++ )) ; do
                n=$((1 << en))
                dir=$root/results/$threads/$app/$n-$t/$i
                rm -rf $dir/traces
                mkdir -p $dir/traces
                if ! (test -f $dir/out && grep -q "$keyword" $dir/out) ; then
                    echo "running $dir"
                    echo "MPCFRAMEWORK_OMP_TASK_TRACEDIR=$dir/traces MPCFRAMEWORK_OMP_TASK_TRACE=1 LIBOMP_USE_HIDDEN_HELPER_TASK=1 LIBOMP_NUM_HIDDEN_HELPER_THREADS=$threads MPCFRAMEWORK_LAUNCH_MPCRUN_CORE=$threads OMP_NUM_THREADS=$threads OMP_PLACES=\"cores($threads)\" nsys profile --stats=true --output=$dir/nsys-report time -v \"$app\" $n $t $b $p $check > $dir/out 2> $dir/err" > $dir/cmd
                    MPCFRAMEWORK_OMP_TASK_TRACEDIR=$dir/traces MPCFRAMEWORK_OMP_TASK_TRACE=1 LIBOMP_USE_HIDDEN_HELPER_TASK=1 LIBOMP_NUM_HIDDEN_HELPER_THREADS=$threads MPCFRAMEWORK_LAUNCH_MPCRUN_CORE=$threads OMP_NUM_THREADS=$threads OMP_PLACES="cores($threads)" nsys profile --stats=true --output=$dir/nsys-report time -v "$app" $n $t $b $p $check > $dir/out 2> $dir/err
                    #srun -p rome python3 ~/WORKDIR/these/tools/mpc/task/trace_stats.py $dir/traces $dir/mpc-stats && rm -rf $dir/traces &
                else
                    echo "$dir/out already exists"
                fi
            done # for iter
        done # for t
    done # for en
done # for threads
