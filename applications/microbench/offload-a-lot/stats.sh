#!/bin/sh

# NUMBER OF MEASURES PER POINT
ITER0=0
ITERN=5

root=$(pwd)

# enable tracing
trace=0
keyword="took"

export MPCFRAMEWORK_LAUNCH_DEBUG_VERBOSITY=1
export MPCFRAMEWORK_OMP_THREAD_BINDINGS=1

echo "app:threads:time:gflops:n:t:i"

for app in main-mpc main-llvm main-nv ; do
    for threads in 2 4 8 12 16 ; do
        for en in {4..16} ; do
            for t in 64 ; do
                n=$[1 << en]
                for (( i=$ITER0; i < $ITERN; i++ )) ; do
                    dir=$root/results/$threads/$app/$n-$t/$i
                    if test -f $dir/out && grep -q "$keyword" $dir/out ; then
                        tim=$(cat $dir/out | grep took | cut -d ' ' -f 3)
                        gflops=$(cat $dir/out | grep GFlop | cut -d ' ' -f 1)
                        echo $app:$threads:$tim:$gflops:$n:$t:$i
                    fi
                done # for i
            done # for t
        done # for en
    done # threads
done # for iter
