# include <assert.h>
# include <omp.h>
# include <math.h>
# include <stdio.h>
# include <stdint.h>
# include <stdlib.h>

#ifdef MPC_TRACE
# include <mpc_omp_task_trace.h>
char TASK_LABEL[MPC_OMP_TASK_LABEL_MAX_LENGTH];
# define SET_LABEL(...) do {                                                                    \
                            snprintf(TASK_LABEL, MPC_OMP_TASK_LABEL_MAX_LENGTH, __VA_ARGS__);   \
                            mpc_omp_task_label(TASK_LABEL);                                     \
                        } while(0)

# define MPC_PREPARE_TARGET(L)  do {                        \
                                    mpc_omp_task_fiber();   \
                                    mpc_omp_task_untied();  \
                                    SET_LABEL(L);           \
                                } while (0)
#else
# define SET_LABEL(...)
# define MPC_PREPARE_TARGET(...)
#endif

# include <cuda.h>
# include <cuda_runtime.h>

/** From "Concurrent Execution of Deferred OpenMP Target Tasks with Hidden Helper Threads" */
# define daxpy(A, X, Y, I0, IF) for (uint64_t _i = I0 ; _i < IF ; ++_i)     \
                                    for (uint64_t _j = 0 ; _j <= _i ; ++_j) \
                                        Y[_i] = Y[_i] + A * X[_j];

static inline void
K(double a, double * X, double * Y, uint64_t n)
{
    MPC_PREPARE_TARGET("K");
    # pragma omp target teams distribute parallel for nowait
    daxpy(a, X, Y, 0, n);
}

static void
B1(double a, double * x, double * y, uint64_t T, uint64_t n)
{
    for (uint64_t t = 0; t < T ; ++t)
    {
        SET_LABEL("K(t=%lu)", t);
        K(a, x, y, n);
    }

    # pragma omp taskwait
}

static void
B3(double a, double * x, double * y, uint64_t T, uint64_t n)
{
    volatile int done = 0;
    # pragma omp parallel
    {
#ifdef MPC_TRACE
        mpc_omp_task_trace_begin();
#endif /* MPC_TRACE */
        if (omp_get_thread_num() == 0)
        {
            double t0 = omp_get_wtime();
            B1(a, x, y, T, n);
            double tf = omp_get_wtime();
            printf("B3 took %lf s.\n", tf - t0);
            printf("%lf GFlop/s\n", 2 * T * n*(n+1)/2 / (tf - t0) / 1e9);
        }
        else
        {
            while (!done);
        }
#ifdef MPC_TRACE
        mpc_omp_task_trace_end();
#endif /* MPC_TRACE */
    }
}

static void
B5(double a, double * x, double * y, uint64_t T, uint64_t n)
{
    # pragma omp parallel
    {
#ifdef MPC_TRACE
        mpc_omp_task_trace_begin();
#endif /* MPC_TRACE */
        # pragma omp single
        {
            double t0 = omp_get_wtime();
            B1(a, x, y, T, n);
            double tf = omp_get_wtime();
            printf("B5 took %lf s.\n", tf - t0);
            printf("%lf GFlop/s\n", 2 * T * n*(n+1)/2 / (tf - t0) / 1e9);
        }
#ifdef MPC_TRACE
        mpc_omp_task_trace_end();
#endif /* MPC_TRACE */
    }
}

/* Use to compare with LLVM HHT */
static void
B6(double a, double * x, double * y, uint64_t T, uint64_t n)
{
    # pragma omp parallel
    {
#ifdef MPC_TRACE
        mpc_omp_task_trace_begin();
#endif /* MPC_TRACE */
        # pragma omp single
        {
            double t0 = omp_get_wtime();
            for (uint64_t t = 0; t < T ; ++t)
            {
                MPC_PREPARE_TARGET("S");
                # pragma omp target update to(y[t*n:n]) nowait depend(out: y[t*n])

                MPC_PREPARE_TARGET("C");
                # pragma omp target teams distribute parallel for nowait depend(out: y[t*n])
                daxpy(a, x, y, t*n, (t+1)*n);

                MPC_PREPARE_TARGET("R");
                # pragma omp target update from(y[t*n:n]) nowait depend(out: y[t*n])
            }
            printf("Task generation %lf\n", omp_get_wtime() - t0);
            # pragma omp taskwait
            puts("taskwaited");
            double tf = omp_get_wtime();
            printf("B6 took %lf s.\n", tf - t0);
            uint64_t m = T * n;
            printf("%lf GFlop/s\n", 2 * m*(m+1)/2 / (tf - t0) / 1e9);
        }
#ifdef MPC_TRACE
        mpc_omp_task_trace_end();
#endif /* MPC_TRACE */
    }
}

/* Use to generate overlap gantt on 2 threads */
static void
B7(double a, double * x, double * y, uint64_t T, uint64_t n)
{
    # pragma omp parallel
    {
#ifdef MPC_TRACE
        mpc_omp_task_trace_begin();
#endif /* MPC_TRACE */
        # pragma omp single
        {
            double t0 = omp_get_wtime();
            for (uint64_t t = 0; t < T ; ++t)
            {
                MPC_PREPARE_TARGET("GPU");
                mpc_omp_task_priority(1);
                # pragma omp target teams distribute parallel for nowait depend(out: y[t*n])
                daxpy(a, x, y, t*n, (t+1)*n);

                MPC_PREPARE_TARGET("CPU");
                mpc_omp_task_priority(0);
                # pragma omp task
                {
                    for (uint64_t k = 0 ; k < 220 * n ; ++k)
                        if (k == k * k) x = k;
                }
            }
            printf("Task generation %lf\n", omp_get_wtime() - t0);
            # pragma omp taskwait
            puts("taskwaited");
            double tf = omp_get_wtime();
            printf("B6 took %lf s.\n", tf - t0);
            uint64_t m = T * n;
            printf("%lf GFlop/s\n", 2 * m*(m+1)/2 / (tf - t0) / 1e9);
        }
#ifdef MPC_TRACE
        mpc_omp_task_trace_end();
#endif /* MPC_TRACE */
    }
}

static double
mix(double * x, double * y, uint64_t nelements)
{
    double m = 0.0;
    # pragma omp parallel for reduction(+:m)
    for (uint64_t i = 0 ; i < nelements; ++i)
    {
        m += x[i] * y[i];
    }
    return m;
}

static void
init(double * x, double * y, uint64_t nelements)
{
    # pragma omp parallel for
    for (uint64_t i = 0 ; i < nelements ; ++i)
    {
        x[i] =       i / (double) nelements;
        y[i] = 1.0 - i / (double) nelements;
    }
}

int
main(int argc, char ** argv)
{
    if (argc != 6)
    {
        fprintf(stderr, "usage: %s [n] [T]\n", argv[0]);
        fprintf(stderr, "   n - the task run 'n' iterations\n");
        fprintf(stderr, "   T - number of tasks to run\n");
        fprintf(stderr, "   B - bench ID (3, 5, 6 or 7)\n");
        fprintf(stderr, "   p - 0 for pageable, 1 for pinned memory allocation\n");
        fprintf(stderr, "   c - 1 to enable correctness checking\n");
        return 1;
    }
    uint64_t n = atoi(argv[1]);
    uint64_t T = atoi(argv[2]);
    int B = atoi(argv[3]);
    int p = atoi(argv[4]);
    int check = atoi(argv[5]);
    double a = 3.14159265359;
    double * x, * y;
    uint64_t nelements = (B == 6) ? n*T : n;
    switch (p)
    {
        case (0):
        {
            x = (double *) malloc(nelements * sizeof(double));
            y = (double *) malloc(nelements * sizeof(double));
            puts("Using 'malloc'");
            break ;
        }
        default:
        {
            cudaMallocHost((void **) &x, nelements * sizeof(double));
            cudaMallocHost((void **) &y, nelements * sizeof(double));
            puts("Using 'cudaMallocHost'");
            break ;
        }
    }
    init(x, y, nelements);
//    for (uint64_t i = 0 ; i < n * T ; ++i)  printf("y[%lu]=%lf ; x[%lu]=%lf\n", i, y[i], i, x[i]);
    double mix1 = 0.0;
    if (check)
    {
        puts("CPU...");
        switch (B)
        {
            case (3):
            case (5):
            {
                daxpy(a, x, y, 0, n);
                mix1 = T * mix(x, y, nelements);
                break ;
            }

            case (6):
            case (7):
            {
                daxpy(a, x, y, 0, nelements);
                mix1 = mix(x, y, nelements);
                break ;
            }

            default:
            {
                break ;
            }
        }
    }

    init(x, y, nelements);
    # pragma omp target enter data map(alloc: a, x[0:nelements], y[0:nelements])
    # pragma omp target update to(a, x[0:nelements])

    puts("GPU...");
    switch (B)
    {
        case (3):
        {
            B3(a, x, y, T, n);
            break ;
        }

        case (5):
        {
            B5(a, x, y, T, n);
            break ;
        }

        case (6):
        {
            B6(a, x, y, T, n);
            break ;
        }

        case (7):
        {
            B7(a, x, y, T, n);
            break ;
        }

        default:
        {
            break ;
        }
    }

    # pragma omp target exit data map(release: a, x[0:nelements], y[0:nelements])

    if (check)
    {
        double mix2 = mix(x, y, nelements);
        printf("mix1 = %lf ; mix2 = %lf\n", mix1, mix2);
        assert(fabs(mix1 - mix2) < 10e-6);
    }

    switch (p)
    {
        case (0):
        {
            free(x);
            free(y);
            break ;
        }
        default:
        {
            cudaFreeHost(x);
            cudaFreeHost(y);
            break ;
        }
    }
    return 0;
}
