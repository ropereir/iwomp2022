# include <assert.h>
# include <omp.h>
# include <stdio.h>
# include <stdint.h>
# include <stdlib.h>

#ifdef TRACE
# include <mpc_omp_task_trace.h>
# define SET_LABEL(X) mpc_omp_task_label(X)
# define SET_FIBER()  mpc_omp_task_fiber()
#else
# define SET_LABEL(X)
# define SET_FIBER()
#endif

# define TASK_BODY(N, X, Y) for (uint64_t i = 0 ; i < N ; ++i)      \
                                for (uint64_t j = 0 ; j <= i ; ++j) \
                                    Y[i] = Y[i] + a * X[j];

int
main(int argc, char ** argv)
{
    if (argc != 6)
    {
        fprintf(stderr, "usage: %s [mode] [cpu_work] [gpu_work] [n] [order]\n", argv[0]);
        fprintf(stderr, "       mode - 0 for blocking, 1 for non-blocking\n");
        fprintf(stderr, "   cpu_work - the cpu task runs 'cpu_work x cpu_work' loop iterations\n");
        fprintf(stderr, "   gpu_work - the gpu task runs 'gpu_work x gpu_work' loop iterations\n");
        fprintf(stderr, "          n - number of tasks each of type to spawn\n");
        fprintf(stderr, "      order - 0 to create the tasks cpu > gpu - 1 for gpu > cpu\n");
        return 1;
    }
    int mode = atoi(argv[1]);
    uint64_t cpu_work = atoi(argv[2]);
    uint64_t gpu_work = atoi(argv[3]);
    int n = atoi(argv[4]);
    int order = atoi(argv[5]);

    double a = 3.14159265359;
    double * xcpu = (double *) malloc(cpu_work * sizeof(double));
    double * ycpu = (double *) malloc(cpu_work * sizeof(double));
    double * xgpu = (double *) malloc(gpu_work * sizeof(double));
    double * ygpu = (double *) malloc(gpu_work * sizeof(double));

    # pragma omp target enter data map(alloc: a, gpu_work, xgpu[0:gpu_work], ygpu[0:gpu_work])
    # pragma omp target update to(a, gpu_work)

    # pragma omp parallel
    {
#ifdef TRACE
        mpc_omp_task_trace_begin();
#endif /* TRACE */
        if (mode == 0)
        {
            # pragma omp single
            {
                printf("Blocking mode\n");

                if (order == 0)
                {
                    double t2 = omp_get_wtime();
                    SET_LABEL("CPU");
                    # pragma omp task if(0)
                        TASK_BODY(cpu_work, xcpu, ycpu);
                    # pragma omp taskwait
                    double t3 = omp_get_wtime();
                    printf("  %lf s. (CPU)\n", t3 - t2);

                    double t0 = omp_get_wtime();
                    SET_LABEL("GPU");
                    # pragma omp target teams distribute parallel for
                        TASK_BODY(gpu_work, xgpu, ygpu);
                    double t1 = omp_get_wtime();
                    printf("  %lf s. (GPU)\n", t1 - t0);
                }
                else
                {
                    double t0 = omp_get_wtime();
                    SET_LABEL("GPU");
                    # pragma omp target teams distribute parallel for
                        TASK_BODY(gpu_work, xgpu, ygpu);
                    double t1 = omp_get_wtime();
                    printf("  %lf s. (GPU)\n", t1 - t0);

                    double t2 = omp_get_wtime();
                    SET_LABEL("CPU");
                    # pragma omp task if(0)
                        TASK_BODY(cpu_work, xcpu, ycpu);
                    # pragma omp taskwait
                    double t3 = omp_get_wtime();
                    printf("  %lf s. (CPU)\n", t3 - t2);
                }
            }
        }
        else
        {
            if (omp_get_thread_num() == 0)  printf("Non-blocking mode\n");
            double t0 = omp_get_wtime();
            # pragma omp single
            {
                for (int k = 0 ; k < n ; ++k)
                {
                    if (order == 0)
                    {
                        SET_LABEL("CPU");
                        # pragma omp task
                            TASK_BODY(cpu_work, xcpu, ycpu);

                        SET_FIBER(); SET_LABEL("GPU");
                        # pragma omp target teams distribute parallel for nowait
                            TASK_BODY(gpu_work, xgpu, ygpu);
                    }
                    else
                    {
                        SET_FIBER(); SET_LABEL("GPU");
                        # pragma omp target teams distribute parallel for nowait
                            TASK_BODY(gpu_work, xgpu, ygpu);

                        SET_LABEL("CPU");
                        # pragma omp task
                            TASK_BODY(cpu_work, xcpu, ycpu);
                    }
                }
                /* this should not be necessary, but nvidia openmp deadlocks
                 * the implicit 'single' region barrier */
                # pragma omp taskwait
            }
            if (omp_get_thread_num() == 0)  printf("  %lf s.\n", omp_get_wtime() - t0);
        }
#ifdef TRACE
        mpc_omp_task_trace_end();
#endif /* TRACE */
    }
    # pragma omp target exit data map(release: gpu_work, xgpu[0:gpu_work], ygpu[0:gpu_work])
    return 0;
}
