#ifndef INTEROP_H
# define INTEROP_H

# if USE_MPI_INTEROP
#  include "interop_mpi.h"
# else
#  define MPIX_Wait     MPI_Wait
#  define MPIX_Waitall  MPI_Waitall
# endif

#endif /* INTEROP_H */
