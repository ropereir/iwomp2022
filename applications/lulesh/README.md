# Usage
Usage: ./lulesh-omp-for [opts] - where [opts] is one or more of:
- -q              : quiet mode - suppress all stdout
- -i <iterations> : number of cycles to run
- -s <size>       : length of cube mesh along side
- -r <numregions> : Number of distinct regions (def: 11)
- -b <balance>    : Load balance between regions of a domain (def: 1)
- -c <cost>       : Extra cost of more expensive regions (def: 1)
- -f <numfiles>   : Number of files to split viz dump into (def: (np+10)/9)
- -p              : Print out progress
- -v              : Output viz file (requires compiling with -DVIZ_MESH
- -te             : Number of tasks for element-wise loops (MPC).
- -tn             : Number of tasks for node-wise loops (MPC).
- -h              : This message

# Memory consumption
Memory complexity is O(s^3) - here are a few memory usage records.

| -s value         | 100 | 128 | 150 | 196 |
|------------------|-----|-----|-----|-----|
| Memory used (GB) | 0.9 | 1.8 | 3.3 | 6.1 |

A good estimator is `925 . s**3` bytes.

# Versions
This repository contains 2 versions, which both supports MPI parallelization.

For every versions
- The correctness has been verified using section 4.3 of the LULESH 2.0 report.
- The application has been modified accordingly to the section « 6.3. Changes not to make to LULESH ». That is, the mesh representation, the loop structures, and extra computation were not changed.
- Memory is now being pre-allocated (`Allocate` moved outside of loops) - in order to reduce the footprint on execution time.

## BSP / parallel-for
This version uses a fork-join programming model, through OpenMP parallel-for.
It corresponds to the version from the original repository, with memory pre-allocation.
This optimization improves performances by about 20% - and it reduces memory allocation noises.

## Tasks

### Current version
This version uses the task programming model, through OpenMP task specifications.
However, a few optimizations have been added, to enable better performances, that is:
- 1) task dependencies pre-allocations
- 2) task dependencies redundancy pre-computation
- 3) `inoutset` reset mecanism, to remove useless inter-iterations arcs while creating multiple `inoutset` dependencies on the same data, without `inout` in between.

While this MPC-specific version could be ported to standard OpenMP, there is none yet available.
The main reasons are:
- 1) it can be achieved through OpenMP `depobj` - but implementation and support is compiler-dependant, and we faced many compiler issues (such as « cannot use iterators on depobj »)
- 2) with current OpenMP state, this should be done by inserting instruction on compile-time when generating « depobj » - but yet, no compilers do so.
- 3) there is no easy solution to this. Users could use « fake » data-dependency to reach similar behaviour... but this is very wobbly since it will flood the runtime with different dependencies for the same data. It may also lead to unexpected behaviour, if colliding with another task dependency (we are talking about more than 1 million arcs in a "normal run").

### Nanos6, OpenMP standard
Nanos6 (OmpSs-2) version is under-developpement, still some race condition to fix out.
Standard OpenMP version is also planned, similarly to HPCCG.

### Other versions
Commit 05485c553be9fe213760aee5ef7efcbf6e312cb8 contains a version where some loops are merged.
This version greatly accelerate the task graph generation time, without loosing parallelism.

## Task granularity
The `-te` and `-tn` parameters indicates the number of tasks that should respectively decompose an element-wise loop, and a node-wise loop.
To ensure enough parallelism expression, they should be greater than the number of local threads. However, we also want a task grain between 0.1 ms. and 100ms. - otherwise the runtime overhead will prevale.

Here are some ~80% efficiency scenarios.

| Field                             |           |           |           |
|-----------------------------------|-----------|-----------|-----------|
| -s value                          | 120       | 120       | 120       |
| -i value                          | 16        | 16        | 16        |
| -te value (= -tn)                 | 128       | 256       | 128       |
| -b value                          | 1         | 1         | 4         |
| -c value                          | 1         | 1         | 8         |
| Graph generation (s.)             | 0.4       | 1.4       | 0.5       |
| Graph execution (s.)              | 3.17      | 2.97      | 7.45      |
| Median task duration (ms.)        | 0.50      | 0.25      | 1.42      |
| Number of tasks                   | 42,561    | 84,113    | 52,257    |
| Number of arcs                    | 1,114,345 | 2,259,721 | 1,696,393 |
| Time inside tasks (%)             | 82.3%     | 79.8%     | 82.7%     |
| Time outside tasks - overhead (%) | 12.0%     | 17.7%     | 4.8%      |
| Time outside tasks - idle (%)     | 5.7%      | 2.5%      | 12.4%     |
