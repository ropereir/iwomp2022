# Common code
COMMON_CFLAGS=-Wall -Wextra -I common/ -g # -DTRACE
#COMMON_CFLAGS+=-Wno-unused-variable -Wno-comment -Wno-pointer-arith
#COMMON_CFLAGS+=-Wno-cast-function-type
#COMMON_CFLAGS+=-Wno-literal-suffix
#COMMON_CFLAGS+=-Wno-reserved-user-defined-literal -Wno-bad-function-cast
COMMON_LDFLAGS=-lm
#COMMON_LDFLAGS+=-lstdc++
COMMON_SRC=common/lulesh-init.cc common/lulesh-util.cc common/lulesh-viz.cc

help:
	@echo "usage: make [seq|omp|omp-mpi|omp-task|omp-task-mpi|oss|oss-mpi|mpc|mpc-mpi|mpc-mpi-ext]"

##################################
# -------- Sequential  --------- #
##################################
SEQ_CXX=g++
SEQ_CFLAGS=-DUSE_MPI=0 -DUSE_TAMPI=0 -DUSE_SIMPLE=0 -DUSE_MPC=0
SEQ_LDFLAGS=
SEQ_SRC=$(COMMON_SRC) omp/lulesh-for.cc
SEQ_OBJ=$(SEQ_SRC:.cc=.seq.o)
SEQ_TARGET=lulesh-seq

%.seq.o: %.cc
	$(SEQ_CXX) $(COMMON_CFLAGS) $(SEQ_CFLAGS) -o $@ -c $<

$(SEQ_TARGET): $(SEQ_OBJ)
	$(SEQ_CXX) $(COMMON_CFLAGS) $(SEQ_CFLAGS) $(SEQ_OBJ) $(COMMON_LDFLAGS) $(SEQ_LDFLAGS) -o $(SEQ_TARGET)

seq: $(SEQ_TARGET)

##################################
# ------------ OMP ------------- #
##################################
OMP_CXX=clang++
OMP_CFLAGS=-DUSE_MPI=0 -DUSE_TAMPI=0 -DUSE_SIMPLE=0 -DUSE_MPC=0 -fopenmp -O3
#OMP_CFLAGS+=-fno-mpc-privatize
OMP_LDFLAGS=
OMP_SRC=$(COMMON_SRC) omp/lulesh-for.cc
OMP_OBJ=$(OMP_SRC:.cc=.omp.$(OMP_CXX).o)
OMP_TARGET=lulesh-omp-for-$(OMP_CXX)

%.omp.$(OMP_CXX).o: %.cc
	$(OMP_CXX) $(COMMON_CFLAGS) $(OMP_CFLAGS) -o $@ -c $<

$(OMP_TARGET): $(OMP_OBJ)
	$(OMP_CXX) $(COMMON_CFLAGS) $(OMP_CFLAGS) $(OMP_OBJ) $(COMMON_LDFLAGS) $(OMP_LDFLAGS) -o $(OMP_TARGET)

omp: $(OMP_TARGET)

##################################
# --------- OMP + MPI ---------- #
##################################
OMP_MPI_MPICXX=I_MPI_CXX="$(OMP_CXX)" MPICH_CXX="$(OMP_CXX)" OMPI_CXX="$(OMP_CXX)" mpicxx
OMP_MPI_CFLAGS=-DUSE_MPI=1 -DUSE_TAMPI=0 -DUSE_SIMPLE=1 -DUSE_MPC=0 -fopenmp -O3
#OMP_MPI_CFLAGS+=-fno-mpc-privatize
OMP_MPI_LDFLAGS=

# parallel for
OMP_MPI_FOR_TARGET=lulesh-omp-mpi-for-$(OMP_CXX)
OMP_MPI_FOR_SRC=$(OMP_SRC) common/lulesh-comm-simple.cc
OMP_MPI_FOR_OBJ=$(OMP_MPI_FOR_SRC:.cc=.omp.mpi.$(OMP_CXX).o)

%.omp.mpi.$(OMP_CXX).o: %.cc
	$(OMP_MPI_MPICXX) $(COMMON_CFLAGS) $(OMP_MPI_CFLAGS) -o $@ -c $<

$(OMP_MPI_FOR_TARGET): $(OMP_MPI_FOR_OBJ)
	$(OMP_MPI_MPICXX) $(COMMON_CFLAGS) $(OMP_MPI_CFLAGS) $(OMP_MPI_FOR_OBJ) $(COMMON_LDFLAGS) $(OMP_MPI_LDFLAGS) -o $(OMP_MPI_FOR_TARGET)

omp-mpi: $(OMP_MPI_FOR_TARGET)

# task
OMP_MPI_TASK_TARGET=lulesh-omp-mpi-task
OMP_MPI_TASK_SRC=$(COMMON_SRC) omp/lulesh-t.cc common/lulesh-comm-task.cc
OMP_MPI_TASK_OBJ=$(OMP_MPI_TASK_SRC:.cc=.omp.mpi.o)

$(OMP_MPI_TASK_TARGET): $(OMP_MPI_TASK_OBJ)
	$(OMP_MPI_MPICXX) $(COMMON_CFLAGS) $(OMP_MPI_CFLAGS) $(OMP_MPI_TASK_OBJ) $(COMMON_LDFLAGS) $(OMP_MPI_LDFLAGS) -o $(OMP_MPI_TASK_TARGET)

##################################
# --------- OMP TASK ----------- #
##################################
OMP_TASK_CXX=clang++
OMP_TASK_CFLAGS= -DUSE_MPI=0 -DUSE_TAMPI=0 -DUSE_SIMPLE=0 -DUSE_MPC=0 -fopenmp
OMP_TASK_CFLAGS+=-DTRACE
OMP_TASK_CFLAGS+=-fopenmp-targets=nvptx64-nvidia-cuda
#OMP_TASK_CFLAGS+=-fopenmp-targets=x86_64-unknown-linux-gnu
OMP_TASK_CFLAGS+=-Xopenmp-target
#OMP_TASK_CFLAGS+=-march=sm_60
#OMP_TASK_CFLAGS+=-march=sm_75
OMP_TASK_CFLAGS+=-march=sm_80
#OMP_TASK_CFLAGS+=-march=x86-64
#OMP_TASK_CFLAGS+=-stdlib=libc++ -Wl,-rpath,/home/mferat/llvm/13.0.1-libc/lib
OMP_TASK_LDFLAGS=
OMP_TASK_LDFLAGS+=-L /ccc/work/cont001/ocre/pereirar/these/mpc-install/urca/lrc/iwomp22/lib -L/ccc/work/cont001/sanl_ipc/sanl_ipc/install/llvm/14.x.target.a100/lib -lomptarget -L /ccc/products/gcc-11.2.0/system/default/lib64/ -lstdc++

OMP_TASK_SRC=$(COMMON_SRC) omp-task/lulesh.cc
OMP_TASK_OBJ=$(OMP_TASK_SRC:.cc=.omp-task.o)
OMP_TASK_TARGET=lulesh-omp-task-$(OMP_TASK_CXX)

%.omp-task.o: %.cc
	$(OMP_TASK_CXX) $(COMMON_CFLAGS) $(OMP_TASK_CFLAGS) -o $@ -c $<

$(OMP_TASK_TARGET): $(OMP_TASK_OBJ)
	$(OMP_TASK_CXX) $(COMMON_CFLAGS) $(OMP_TASK_CFLAGS) $(OMP_TASK_OBJ) $(COMMON_LDFLAGS) $(OMP_TASK_LDFLAGS) -o $(OMP_TASK_TARGET)

omp-task: $(OMP_TASK_TARGET)

##################################
# ------------ MPC ------------- #
##################################
CLANG_DIR=$(shell which clang | rev | cut -d '/' -f 3- | rev)
MPC_DIR=$(shell which mpc_cc | rev | cut -d '/' -f 3- | rev)
GCC_DIR=$(shell which g++ | rev | cut -d '/' -f 3- | rev)
MPC_CXX_CC=clang++
MPC_CXX=mpc_cxx -cc=$(MPC_CXX_CC)

MPC_CFLAGS=-Wall -Wextra -fopenmp -O3
MPC_CFLAGS+=-DUSE_MPI=0 -DUSE_TAMPI=0 -DUSE_SIMPLE=0 -DUSE_MPC=1 -fopenmp
MPC_CFLAGS+=-DTRACE
MPC_CFLAGS+=-g
MPC_CFLAGS+=-fopenmp-targets=nvptx64-nvidia-cuda -Xopenmp-target
#MPC_CFLAGS+=-march=sm_75
MPC_CFLAGS+=-march=sm_80
MPC_LDFLAGS=-L$(MPC_DIR)/lib
#MPC_LDFLAGS+=-L/usr/local/cuda/lib64 -lcudart
MPC_LDFLAGS+=-L/ccc/products/nvhpc-22.2/system/default/Linux_x86_64/22.2/cuda/lib64 -lcudart
MPC_LDFLAGS+=-L$(MPC_DIR)/lib
MPC_LDFLAGS+=-L$(CLANG_DIR)/lib -lomptarget
MPC_LDFLAGS+=-L$(GCC_DIR)/lib64/ -lstdc++



MPC_SRC=$(COMMON_SRC)

%.mpc.o: %.cc
	$(MPC_CXX) $(COMMON_CFLAGS) $(MPC_CFLAGS) -o $@ -c $<

# tasks
MPC_T_TARGET=lulesh-mpc-t-$(MPC_CXX_CC)
MPC_T_SRC=$(MPC_SRC) mpc/lulesh.cc
MPC_T_OBJ=$(MPC_T_SRC:.cc=.mpc.o)

$(MPC_T_TARGET): $(MPC_T_OBJ)
	$(MPC_CXX) $(COMMON_CFLAGS) $(MPC_CFLAGS) $(MPC_T_OBJ) $(COMMON_LDFLAGS) $(MPC_LDFLAGS) -o $(MPC_T_TARGET)

mpc: $(MPC_T_TARGET)

##################################
# ---------- MPC-MPI ----------- #
##################################
#MPC_MPICXX=I_MPI_CXX=$(MPC_CXX) MPICH_CXX=$(MPC_CXX) OMPI_CXX=$(MPC_CXX) mpicxx
MPC_MPI_CXX=mpc_cxx -cc=g++
MPC_MPI_MPICXX=mpc_cxx

MPC_MPI_CFLAGS=-DUSE_MPI=1 -DUSE_MPI_INTEROP=0 -DUSE_SIMPLE=1 -DUSE_MPC=1 -fopenmp #-DTRACE
MPC_MPI_LDFLAGS=
MPC_MPI_SRC=$(COMMON_SRC) mpc/lulesh-comm-simple-task.cc

%.mpc.mpi.o: %.cc
	$(MPC_MPI_MPICXX) $(COMMON_CFLAGS) $(MPC_MPI_CFLAGS) -o $@ -c $<

# tasks
MPC_MPI_T_TARGET=lulesh-mpc-mpi-t-trace
MPC_MPI_T_SRC=$(MPC_MPI_SRC) mpc/lulesh.cc
MPC_MPI_T_OBJ=$(MPC_MPI_T_SRC:.cc=.mpc.mpi.o)

$(MPC_MPI_T_TARGET): $(MPC_MPI_T_OBJ)
	$(MPC_MPI_MPICXX) $(COMMON_CFLAGS) $(MPC_MPI_CFLAGS) $(MPC_MPI_T_OBJ) $(COMMON_LDFLAGS) $(MPC_MPI_LDFLAGS) -o $(MPC_MPI_T_TARGET)

MPC_MPI_TARGET=$(MPC_MPI_T_TARGET)
MPC_MPI_OBJ=$(MPC_MPI_T_OBJ)

mpc-mpi: $(MPC_MPI_TARGET)

##################################
# --------- MPC-OMPI ----------- #
##################################
MPC_MPI_CXX_CC=clang++
MPC_MPI_EXT_CXX="mpc_cxx -cc=$(MPC_MPI_CXX_CC)"
MPC_MPI_EXT_MPICXX=I_MPI_CXX=$(MPC_MPI_EXT_CXX) MPICH_CXX=$(MPC_MPI_EXT_CXX) OMPI_CXX=$(MPC_MPI_EXT_CXX) mpicxx

MPC_MPI_EXT_CFLAGS=-Wall -Wextra -O3
MPC_MPI_EXT_CFLAGS+=-DUSE_MPI=1 -DUSE_MPI_INTEROP=1 -DUSE_SIMPLE=1 -DUSE_MPC=1 -fopenmp
MPC_MPI_EXT_CFLAGS+=-DTRACE
#MPC_MPI_EXT_CFLAGS+=-I/home/mferat/llvm_src/llvm-project/openmp/libomptarget/include
MPC_MPI_EXT_CFLAGS+=-fopenmp-targets=nvptx64-nvidia-cuda -Xopenmp-target
#MPC_MPI_EXT_CFLAGS+=-march=sm_60
#MPC_MPI_EXT_CFLAGS+=-march=sm_75
MPC_MPI_EXT_CFLAGS+=-march=sm_80
MPC_MPI_EXT_LDFLAGS=-L$(MPC_DIR)/lib
MPC_MPI_EXT_LDFLAGS+=-L$(CLANG_DIR)/lib -lomptarget
#MPC_MPI_EXT_LDFLAGS+=-L/apps/2021/gcc/10.2/lib64 -lstdc++
MPC_MPI_EXT_LDFLAGS+=-L$(GCC_DIR)/lib64/ -lstdc++
#MPC_MPI_EXT_LDFLAGS+=-L/apps/2021/cuda/11.4/lib64 -lcudart
MPC_MPI_EXT_LDFLAGS+=-L/ccc/products/nvhpc-22.2/system/default/Linux_x86_64/22.2/cuda/lib64 -lcudart

MPC_MPI_EXT_SRC=$(MPC_MPI_SRC)

%.mpc.mpi-ext.o: %.cc
	$(MPC_MPI_EXT_MPICXX) $(COMMON_CFLAGS) $(MPC_MPI_EXT_CFLAGS) -o $@ -c $<

# tasks
MPC_MPI_EXT_T_TARGET=lulesh-mpc-mpi-ext-t#-trace
MPC_MPI_EXT_T_SRC=$(MPC_MPI_SRC) mpc/lulesh.cc
MPC_MPI_EXT_T_OBJ=$(MPC_MPI_EXT_T_SRC:.cc=.mpc.mpi-ext.o)

$(MPC_MPI_EXT_T_TARGET): $(MPC_MPI_EXT_T_OBJ)
	$(MPC_MPI_EXT_MPICXX) $(COMMON_CFLAGS) $(MPC_MPI_EXT_CFLAGS) $(MPC_MPI_EXT_T_OBJ) $(COMMON_LDFLAGS) $(MPC_MPI_EXT_LDFLAGS) -o $(MPC_MPI_EXT_T_TARGET)

mpc-mpi-ext: $(MPC_MPI_EXT_T_TARGET)

######################################
# ------------ OmpSs 2 ------------- #
######################################
OSS_CXX=mcxx
OSS_CFLAGS=-DUSE_MPI=0 -DUSE_TAMPI=0 -DUSE_SIMPLE=0 -DUSE_MPC=0
OSS_CFLAGS+=--ompss-2 --openmp-compatibility
#OSS_CFLAGS+=--keep-all-files
OSS_LDFLAGS=
OSS_SRC=$(COMMON_SRC)

%.oss.o: %.cc
	$(OSS_CXX) $(COMMON_CFLAGS) $(OSS_CFLAGS) -o $@ -c $<

# tasks
OSS_T_TARGET=lulesh-oss-t
OSS_T_SRC=$(OSS_SRC) oss/lulesh.cc
OSS_T_OBJ=$(OSS_T_SRC:.cc=.oss.o)

$(OSS_T_TARGET): $(OSS_T_OBJ)
	$(OSS_CXX) $(COMMON_CFLAGS) $(OSS_CFLAGS) $(OSS_T_OBJ) $(COMMON_LDFLAGS) $(OSS_LDFLAGS) -o $(OSS_T_TARGET)

OSS_TARGET=$(OSS_T_TARGET)
OSS_OBJ=$(OSS_T_OBJ)

oss: $(OSS_TARGET)

######################################
# ---------- OmpSs 2 + MPI --------- #
######################################
OSS_MPICXX=I_MPI_CXX=$(OSS_CXX) MPICH_CXX=$(OSS_CXX) OMPI_CXX=$(OSS_CXX) mpicxx
OSS_MPI_CFLAGS=-DUSE_MPI=1 -DUSE_TAMPI=1 -DUSE_SIMPLE=1 -DUSE_MPC=0
OSS_MPI_CFLAGS+=--ompss-2 --openmp-compatibility -I $(TAMPI_ROOT)/include
OSS_MPI_LDFLAGS=-L $(TAMPI_ROOT)/lib -ltampi
OSS_MPI_SRC=$(COMMON_SRC) oss/lulesh-comm-simple-task.cc

%.oss.mpi.o: %.cc
	$(OSS_MPICXX) $(COMMON_CFLAGS) $(OSS_MPI_CFLAGS) -o $@ -c $<

# tasks
OSS_MPI_T_TARGET=lulesh-oss-mpi-t
OSS_MPI_T_SRC=$(OSS_MPI_SRC) oss/lulesh.cc
OSS_MPI_T_OBJ=$(OSS_MPI_T_SRC:.cc=.oss.mpi.o)

$(OSS_MPI_T_TARGET): $(OSS_MPI_T_OBJ)
	$(OSS_MPICXX) $(COMMON_CFLAGS) $(OSS_MPI_CFLAGS) $(OSS_MPI_T_OBJ) $(COMMON_LDFLAGS) $(OSS_MPI_LDFLAGS) -o $(OSS_MPI_T_TARGET)

OSS_MPI_TARGET=$(OSS_MPI_T_TARGET)
OSS_MPI_OBJ=$(OSS_MPI_T_OBJ)

oss-mpi: $(OSS_MPI_TARGET)



###################################
# ------------ utils ------------ #
###################################
all: $(OMP_TARGET) $(OMP_MPI_FOR_TARGET) $(OMP_MPI_TASK_TARGET) $(OSS_TARGET) $(OSS_MPI_TARGET) $(MPC_T_TARGET) $(MPC_MPI_EXT_T_TARGET)

clean:
	rm -rf $(SEQ_OBJ) $(OMP_OBJ) $(OMP_MPI_FOR_OBJ) $(OMP_TASK_OBJ) $(OSS_OBJ) $(OSS_MPI_OBJ) $(MPC_T_OBJ) $(MPC_MPI_OBJ) $(MPC_MPI_EXT_T_OBJ)

dclean:
	rm -rf *.yaml *.btr *.o *.json mcxx_*.cpp traces*

fclean: clean dclean
	rm -rf $(SEQ_TARGET) $(OMP_TARGET) $(OMP_MPI_FOR_TARGET) $(OMP_TASK_TARGET) $(OSS_TARGET) $(OSS_MPI_TARGET) $(MPC_T_TARGET) $(MPC_TR_TARGET) $(MPC_MPI_TARGET) $(MPC_MPI_EXT_T_TARGET)

re: fclean all
